package de.pat.spring.projectPokemon.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by schwirzke on 07.10.16.
 */
@Entity
@Table(name = "ticket_reservation")
public class Ticket_Reservation implements Serializable{


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ticket_reservation_id;

    @ManyToOne
    Event_Serie event_serie;

    @ManyToOne
    Account account;

    @OneToMany
    Seat_Reservation seat_reservation;

    private boolean paid_status;

    private boolean reservation_status;

    @Temporal(TemporalType.TIMESTAMP)
    private Date Timestamp;

    public Ticket_Reservation() {
    }

    //region Getter & Setter
    public Integer getTicket_reservation_id() {
        return ticket_reservation_id;
    }

    public void setTicket_reservation_id(Integer ticket_reservation_id) {
        this.ticket_reservation_id = ticket_reservation_id;
    }

    public Event_Serie getEvent_serie() {
        return event_serie;
    }

    public void setEvent_serie(Event_Serie event_serie) {
        this.event_serie = event_serie;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Seat_Reservation getSeat_reservation() {
        return seat_reservation;
    }

    public void setSeat_reservation(Seat_Reservation seat_reservation) {
        this.seat_reservation = seat_reservation;
    }

    public boolean isPaid_status() {
        return paid_status;
    }

    public void setPaid_status(boolean paid_status) {
        this.paid_status = paid_status;
    }

    public boolean isReservation_status() {
        return reservation_status;
    }

    public void setReservation_status(boolean reservation_status) {
        this.reservation_status = reservation_status;
    }

    public Date getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(Date timestamp) {
        Timestamp = timestamp;
    }
    //endregion

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Ticket_Reservation that = (Ticket_Reservation) o;


        if (event_serie != null ? !event_serie.equals(that.event_serie) : that.event_serie != null)
            return false;
        if (account != null ? !account.equals(that.account) : that.account != null)
            return false;
        if (seat_reservation != null ? !seat_reservation.equals(that.seat_reservation) : that.seat_reservation != null)
            return false;
        return true;

    }

    @Override
    public int hashCode() {
        int result = event_serie != null ? event_serie.hashCode() : 0;
        result = 31 * result + (account != null ? account.hashCode() : 0);
        result = 31 * result + (seat_reservation != null ? seat_reservation.hashCode() : 0);
        return result;
    }
}
