package de.pat.spring.projectPokemon.model.Location;

import de.pat.spring.projectPokemon.model.Seat_Reservation;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by schwirzke on 07.10.16.
 */
@Entity
@Table(name = "location_rows")
public class Location_Rows implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "location_row_id")
    private Integer lowcation_row_id;

    @Column(name = "row_number")
    private Integer row_number;

    @Column (name = "seat_count")
    private Integer seat_count;

    @ManyToOne
    private Location location;

    @OneToMany
    private Seat_Reservation seat_reservation;

    public Location_Rows() {
    }

    //region Getter & Setter
    public Integer getLowcation_row_id() {
        return lowcation_row_id;
    }

    public void setLowcation_row_id(Integer lowcation_row_id) {
        this.lowcation_row_id = lowcation_row_id;
    }

    public Integer getRow_number() {
        return row_number;
    }

    public void setRow_number(Integer row_number) {
        this.row_number = row_number;
    }

    public Integer getSeat_count() {
        return seat_count;
    }

    public void setSeat_count(Integer seat_count) {
        this.seat_count = seat_count;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Seat_Reservation getSeat_reservation() {
        return seat_reservation;
    }

    public void setSeat_reservation(Seat_Reservation seat_reservation) {
        this.seat_reservation = seat_reservation;
    }
    //endregion

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location_Rows that = (Location_Rows) o;

        if (lowcation_row_id != null ? !lowcation_row_id.equals(that.lowcation_row_id) : that.lowcation_row_id != null)
            return false;
        if (row_number != null ? !row_number.equals(that.row_number) : that.row_number != null) return false;
        if (seat_count != null ? !seat_count.equals(that.seat_count) : that.seat_count != null) return false;
        if (location != null ? !location.equals(that.location) : that.location != null) return false;
        return seat_reservation != null ? seat_reservation.equals(that.seat_reservation) : that.seat_reservation == null;

    }

    @Override
    public int hashCode() {
        int result = lowcation_row_id != null ? lowcation_row_id.hashCode() : 0;
        result = 31 * result + (row_number != null ? row_number.hashCode() : 0);
        result = 31 * result + (seat_count != null ? seat_count.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (seat_reservation != null ? seat_reservation.hashCode() : 0);
        return result;
    }
}
