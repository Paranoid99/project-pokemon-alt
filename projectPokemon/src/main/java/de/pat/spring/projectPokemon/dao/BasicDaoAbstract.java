package de.pat.spring.projectPokemon.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by schwirzke on 05.09.16.
 */
public abstract class BasicDaoAbstract <T> implements BasicDao<T>{


    protected Class<T> clazz;
    protected String orderCrit;


    @Autowired
    private SessionFactory sessionFactory;

    protected final Session getCurrentSession(){
        return sessionFactory.getCurrentSession();
    }

    public Class<T> getClazz() {
        return clazz;
    }

    public void setClazz(Class<T> clazz) {
        this.clazz = clazz;
    }

    public void setOrderCrit(String orderCrit) {

        this.orderCrit = orderCrit;
    }



    @Override
    public T findById(Object id) {

        return (T) getCurrentSession()
                .createCriteria(clazz)
                .add(Restrictions.idEq(id))
                .uniqueResult();
    }

    @Override
    public List<T> findAll() {

        return getCurrentSession()
                .createCriteria(clazz)
                .addOrder(Order.asc(orderCrit))
                .list();
    }

    @Override
    public T getByRestriction(String restriction, Object value) {

        return (T) getCurrentSession()
                .createCriteria(clazz)
                .add(Restrictions.eq(restriction, value))
                .uniqueResult();
    }

    @Override
    public void create(T entity) {
     getCurrentSession().saveOrUpdate(entity);
    }

    @Override
    public void update(T entity) {
       getCurrentSession().merge(entity);
    }

    @Override
    public void delete(T entity) {
        getCurrentSession().delete(entity);
    }
}
