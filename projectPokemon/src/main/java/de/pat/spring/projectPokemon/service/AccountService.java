package de.pat.spring.projectPokemon.service;

import de.pat.spring.projectPokemon.model.Account;

/**
 * Created by schwirzke on 15.09.16.
 */
public interface AccountService {

    void update(Account entity);

    Account findByRestriction(String restriction, Object value);

}
