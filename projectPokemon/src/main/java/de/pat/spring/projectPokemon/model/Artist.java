package de.pat.spring.projectPokemon.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by schwirzke on 07.10.16.
 */
@Entity
@Table (name = "artist")
public class Artist implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "artist_id")
    private Integer artist_id;

    @Column(name = "artist_name", length = 120, nullable = false)
    private String artist_name;

    @Column(name = "artist_description")
    private String artist_description;

    @Column(name = "artist_picture_link")
    private String artist_picture_link;

    public Artist() {
    }

    //region Getter & Setter
    public Integer getArtist_id() {
        return artist_id;
    }

    public String getArtist_name() {
        return artist_name;
    }

    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }

    public String getArtist_description() {
        return artist_description;
    }

    public void setArtist_description(String artist_description) {
        this.artist_description = artist_description;
    }

    public String getArtist_picture_link() {
        return artist_picture_link;
    }

    public void setArtist_picture_link(String artist_picture_link) {
        this.artist_picture_link = artist_picture_link;
    }
    //endregion

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Artist artist = (Artist) o;

        if (artist_id != null ? !artist_id.equals(artist.artist_id) : artist.artist_id != null) return false;
        if (artist_name != null ? !artist_name.equals(artist.artist_name) : artist.artist_name != null) return false;
        if (artist_description != null ? !artist_description.equals(artist.artist_description) : artist.artist_description != null)
            return false;
        return artist_picture_link != null ? artist_picture_link.equals(artist.artist_picture_link) : artist.artist_picture_link == null;

    }

    @Override
    public int hashCode() {
        int result = artist_id != null ? artist_id.hashCode() : 0;
        result = 31 * result + (artist_name != null ? artist_name.hashCode() : 0);
        result = 31 * result + (artist_description != null ? artist_description.hashCode() : 0);
        result = 31 * result + (artist_picture_link != null ? artist_picture_link.hashCode() : 0);
        return result;
    }
}
