package de.pat.spring.projectPokemon.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by schwirzke on 07.10.16.
 */
@Entity
@Table(name = "seat_reservation")
public class Seat_Reservation implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "seat_reservation_id")
    private Integer seat_reservation_id;

    @Column(name = "row_number")
    private Integer row_number;

    @Column(name = "seat_number")
    private Integer seat_number;

    @ManyToOne
    private Location_Rows location_rows;

    @ManyToOne
    private Ticket_Reservation ticket_reservation;

    public Seat_Reservation() {
    }

    //region Getter & Setter
    public Integer getSeat_reservation_id() {
        return seat_reservation_id;
    }

    public void setSeat_reservation_id(Integer seat_reservation_id) {
        this.seat_reservation_id = seat_reservation_id;
    }

    public Integer getRow_number() {
        return row_number;
    }

    public void setRow_number(Integer row_number) {
        this.row_number = row_number;
    }

    public Integer getSeat_number() {
        return seat_number;
    }

    public void setSeat_number(Integer seat_number) {
        this.seat_number = seat_number;
    }

    public Location_Rows getLocation_rows() {
        return location_rows;
    }

    public void setLocation_rows(Location_Rows location_rows) {
        this.location_rows = location_rows;
    }

    public Ticket_Reservation getTicket_reservation() {
        return ticket_reservation;
    }

    public void setTicket_reservation(Ticket_Reservation ticket_reservation) {
        this.ticket_reservation = ticket_reservation;
    }
    //endregion

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Seat_Reservation that = (Seat_Reservation) o;

        if (seat_reservation_id != null ? !seat_reservation_id.equals(that.seat_reservation_id) : that.seat_reservation_id != null)
            return false;
        if (row_number != null ? !row_number.equals(that.row_number) : that.row_number != null) return false;
        if (seat_number != null ? !seat_number.equals(that.seat_number) : that.seat_number != null) return false;
        if (location_rows != null ? !location_rows.equals(that.location_rows) : that.location_rows != null)
            return false;
        return ticket_reservation != null ? ticket_reservation.equals(that.ticket_reservation) : that.ticket_reservation == null;

    }

    @Override
    public int hashCode() {
        int result = seat_reservation_id != null ? seat_reservation_id.hashCode() : 0;
        result = 31 * result + (row_number != null ? row_number.hashCode() : 0);
        result = 31 * result + (seat_number != null ? seat_number.hashCode() : 0);
        result = 31 * result + (location_rows != null ? location_rows.hashCode() : 0);
        result = 31 * result + (ticket_reservation != null ? ticket_reservation.hashCode() : 0);
        return result;
    }
}
