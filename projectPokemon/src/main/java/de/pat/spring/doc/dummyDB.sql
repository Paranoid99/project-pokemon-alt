DROP SCHEMA IF EXISTS dummyDB;
CREATE SCHEMA dummyDB;
USE dummyDB;
SET AUTOCOMMIT=0;

DROP TABLE IF EXISTS `app_user`;
/*All Account's are stored in APP_USER table*/
CREATE TABLE `app_user` (
  id BIGINT NOT NULL AUTO_INCREMENT,
  sso_id VARCHAR(30) NOT NULL,
  password VARCHAR(100) NOT NULL,
  first_name VARCHAR(30) NOT NULL,
  last_name  VARCHAR(30) NOT NULL,
  email VARCHAR(30) NOT NULL,
  state VARCHAR(30) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (sso_id)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

/* Populate APP_USER Table */
INSERT INTO app_user(sso_id, password, first_name, last_name, email, state)
VALUES ('bill','$2a$06$z.bmQIe.cYNUnIpJ/tV0Z.TVdcMl6VsyKY6ZGadaiuEuf8FET8/vW', 'Bill','Watcher','bill@xyz.com', 'Active');

INSERT INTO app_user(sso_id, password, first_name, last_name, email, state)
VALUES ('danny','$2a$06$9l0YIisl/Ns6Z6SC/GvtLekOgvB/Duc5/Ehjk9JLOaVGl12LLb0eq', 'Danny','Theys','danny@xyz.com', 'Active');

INSERT INTO app_user(sso_id, password, first_name, last_name, email, state)
VALUES ('sam','$2a$06$UhGsL3Th1.qDdJ4sjg.L3eXNazkiQJERmbGbVR4Ext9RBArZH5zlG', 'Sam','Smith','samy@xyz.com', 'Active');

INSERT INTO app_user(sso_id, password, first_name, last_name, email, state)
VALUES ('nicole','$2a$06$jPVP577C4kc6wD9FXH7QYOPJp9OtI66H8PqF53AQYwlpLm6bRsx2y', 'Nicole','warner','nicloe@xyz.com', 'Active');

INSERT INTO app_user(sso_id, password, first_name, last_name, email, state)
VALUES ('kenny','$2a$06$rrRu/sgnSHDQfBRRr80h5uaEW6rZgs9A.6ppTdteL0QWuUFAoChcG', 'Kenny','Roger','kenny@xyz.com', 'Active');
COMMIT;


/* USER_PROFILE table contains all possible roles */
DROP TABLE IF EXISTS `user_profile`;
/*All Account's are stored in APP_USER table*/
CREATE TABLE `user_profile` (
  id BIGINT NOT NULL AUTO_INCREMENT,
  type VARCHAR(30) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (type)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

/* Populate USER_PROFILE Table */
INSERT INTO user_profile(type)
VALUES ('USER');

INSERT INTO user_profile(type)
VALUES ('ADMIN');

INSERT INTO user_profile(type)
VALUES ('DBA');
COMMIT;


DROP TABLE IF EXISTS `app_user_user_profile`;
/* JOIN TABLE for MANY-TO-MANY relationship*/
CREATE TABLE `app_user_user_profile`  (
  user_id BIGINT NOT NULL,
  user_profile_id BIGINT NOT NULL,
  PRIMARY KEY (user_id, user_profile_id),
  CONSTRAINT FK_APP_USER FOREIGN KEY (user_id) REFERENCES app_user (id),
  CONSTRAINT FK_USER_PROFILE FOREIGN KEY (user_profile_id) REFERENCES user_profile (id)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

/* Populate JOIN Table */
INSERT INTO app_user_user_profile (user_id, user_profile_id)
  SELECT account.id, profile.id FROM app_user account, user_profile profile
  where account.sso_id='bill' and profile.type='USER';

INSERT INTO app_user_user_profile (user_id, user_profile_id)
  SELECT account.id, profile.id FROM app_user account, user_profile profile
  where account.sso_id='danny' and profile.type='USER';

INSERT INTO app_user_user_profile (user_id, user_profile_id)
  SELECT account.id, profile.id FROM app_user account, user_profile profile
  where account.sso_id='sam' and profile.type='ADMIN';

INSERT INTO app_user_user_profile (user_id, user_profile_id)
  SELECT account.id, profile.id FROM app_user account, user_profile profile
  where account.sso_id='nicole' and profile.type='DBA';

INSERT INTO app_user_user_profile (user_id, user_profile_id)
  SELECT account.id, profile.id FROM app_user account, user_profile profile
  where account.sso_id='kenny' and profile.type='ADMIN';

INSERT INTO app_user_user_profile (user_id, user_profile_id)
  SELECT account.id, profile.id FROM app_user account, user_profile profile
  where account.sso_id='kenny' and profile.type='DBA';
COMMIT;
